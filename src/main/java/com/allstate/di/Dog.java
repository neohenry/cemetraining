package com.allstate.di;

import org.springframework.stereotype.Component;

@Component("dog")
public class Dog implements Pet {
    @Override
    public void feed() {
        System.out.println("Feed the dog");
    }
}
